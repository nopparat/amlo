﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AMLO.Models;

namespace AMLO.DAL
{
    public class SBADAL
    {
        public static void ClearDataInsertData(string _conn, string recordDate, List<kks_amlo_idnumber> kks_amlo_idnumbers, List<kks_amlo_name> kks_amlo_names)
        {
            string res = string.Empty;

            var _kks_amlo_idnumbers = kks_amlo_idnumbers.GroupBy(x => x.xml_file).ToDictionary(x => x.Key, x => x.ToList(), StringComparer.OrdinalIgnoreCase).ToList();
            var _kks_amlo_names = kks_amlo_names.GroupBy(x => x.xml_file).ToDictionary(x => x.Key, x => x.ToList(), StringComparer.OrdinalIgnoreCase);

            
            using (OdbcConnection conn = new OdbcConnection(_conn))
            {
                conn.Open();
                using (OdbcTransaction transaction = conn.BeginTransaction())
                {
                    try
                    {
                        #region Process kks_amlo_idnumbers
                        Console.WriteLine("Process kks_amlo_idnumbers...");
                        foreach (var item in _kks_amlo_idnumbers)
                        {
                            string type = item.Key;

                            // Clear Data kks_amlo_idnumber
                            Console.WriteLine($"Clear Data kks_amlo_idnumber xml_file {type}...");
                            string _query_kks_amlo_idnumber = "delete from kks_amlo_idnumber where xml_file = ? and date_input = ?";
                            using (OdbcCommand cmd = new OdbcCommand(_query_kks_amlo_idnumber, conn, transaction))
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandTimeout = 1800; //int.MaxValue;
                                cmd.Connection = conn;
                                cmd.Parameters.Add("@type", OdbcType.Char).Value = type;
                                cmd.Parameters.Add("@recordDate", OdbcType.Date).Value = recordDate;
                                cmd.ExecuteNonQuery();
                            }

                            Console.WriteLine($"Process INSERT kks_amlo_idnumber...");
                            foreach (var data in item.Value)
                            {
                                Console.WriteLine($"Insert Data entity_id = {data.entity_id}, xml_file = {data.xml_file}, date_input = {data.date_input}...");

                                string _query = "INSERT INTO kks_amlo_idnumber ";
                                _query += "(entity_id,sys_id,id_type,id_value,id_country,id_issuedate,id_expirydate,id_addn_ref,info_source,batch_no,batch_date,xml_file,date_input) ";
                                _query += "VALUES ";
                                _query += "(?,?,?,?,?,?,?,?,?,?,?,?,?) ";

                                using (OdbcCommand cmd = new OdbcCommand(_query, conn, transaction))
                                {
                                    cmd.CommandType = CommandType.Text;
                                    cmd.CommandTimeout = 1800; //int.MaxValue;
                                    cmd.Connection = conn;
                                    cmd.Parameters.Add("entity_id", OdbcType.Char).Value = data.entity_id;
                                    cmd.Parameters.Add("sys_id", OdbcType.Char).Value = data.sys_id;
                                    cmd.Parameters.Add("id_type", OdbcType.Char).Value = data.id_type;
                                    cmd.Parameters.Add("id_value", OdbcType.Char).Value = data.id_value;
                                    cmd.Parameters.Add("id_country", OdbcType.Char).Value = data.id_country;
                                    cmd.Parameters.Add("id_issuedate", OdbcType.Char).Value = data.id_issuedate;
                                    cmd.Parameters.Add("id_expirydate", OdbcType.Char).Value = data.id_expiredate;
                                    cmd.Parameters.Add("id_addn_ref", OdbcType.Char).Value = data.id_addn_ref;
                                    cmd.Parameters.Add("info_source", OdbcType.Char).Value = data.info_source;
                                    cmd.Parameters.Add("batch_no", OdbcType.Char).Value = data.batch_no;
                                    cmd.Parameters.Add("batch_date", OdbcType.Date).Value = data.batch_date;
                                    cmd.Parameters.Add("xml_file", OdbcType.Char).Value = data.xml_file;
                                    cmd.Parameters.Add("date_input", OdbcType.Date).Value = data.date_input;
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }

                        #endregion

                        #region Process kks_amlo_names
                        Console.WriteLine("Process kks_amlo_names...");
                        foreach (var item in _kks_amlo_names)
                        {
                            string type = item.Key;

                            // Clear Data kks_amlo_name
                            Console.WriteLine($"Clear Data kks_amlo_name xml_file {type}...");
                            string _query_kks_amlo_name = "delete from kks_amlo_name where xml_file = ? and date_input = ?";
                            using (OdbcCommand cmd = new OdbcCommand(_query_kks_amlo_name, conn, transaction))
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandTimeout = 1800; //int.MaxValue;
                                cmd.Connection = conn;
                                cmd.Parameters.Add("@type", OdbcType.Char).Value = type;
                                cmd.Parameters.Add("@recordDate", OdbcType.Date).Value = recordDate;
                                cmd.ExecuteNonQuery();
                            }

                            Console.WriteLine($"Process INSERT kks_amlo_name...");
                            foreach (var data in item.Value)
                            {
                                Console.WriteLine($"Insert Data first_name = {data.first_name}, xml_file = {data.xml_file}, date_input = {data.date_input}...");

                                string _query = "INSERT INTO kks_amlo_name ";
                                _query += "(entity_id,sys_id,name_type,first_name,middle_name,surname,suffix,entity_name,english_name,single_string_name,info_source,batch_no,batch_date,xml_file,date_input) ";
                                _query += "VALUES ";
                                _query += "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";

                                using (OdbcCommand cmd = new OdbcCommand(_query, conn, transaction))
                                {
                                    cmd.CommandType = CommandType.Text;
                                    cmd.CommandTimeout = 1800; //int.MaxValue;
                                    cmd.Connection = conn;
                                    cmd.Parameters.Add("entity_id", OdbcType.Char).Value = data.entity_id;
                                    cmd.Parameters.Add("sys_id", OdbcType.Char).Value = data.sys_id;
                                    cmd.Parameters.Add("name_type", OdbcType.Char).Value = data.name_type;
                                    cmd.Parameters.Add("first_name", OdbcType.Char).Value = data.first_name;
                                    cmd.Parameters.Add("middle_name", OdbcType.Char).Value = data.middel_name;
                                    cmd.Parameters.Add("surname", OdbcType.Char).Value = data.surname;
                                    cmd.Parameters.Add("suffix", OdbcType.Char).Value = data.suffix;
                                    cmd.Parameters.Add("entity_name", OdbcType.Char).Value = data.entity_name;
                                    cmd.Parameters.Add("english_name", OdbcType.Char).Value = data.english_name;
                                    cmd.Parameters.Add("single_string_name", OdbcType.Char).Value = data.single_string_name;
                                    cmd.Parameters.Add("info_source", OdbcType.Char).Value = data.info_source;
                                    cmd.Parameters.Add("batch_no", OdbcType.Char).Value = data.batch_no;
                                    cmd.Parameters.Add("batch_date", OdbcType.Date).Value = data.batch_date;
                                    cmd.Parameters.Add("xml_file", OdbcType.Char).Value = data.xml_file;
                                    cmd.Parameters.Add("date_input", OdbcType.Date).Value = data.date_input;
                                    cmd.ExecuteNonQuery();
                                }
                            }
                        }

                        #endregion

                        transaction.Commit();
                        conn.Close();
                    }
                    catch (SqlException ex)
                    {
                        transaction.Rollback();
                        conn.Close();

                        throw new Exception(ex.Message);
                    }
                    finally
                    {
                        conn.Close();
                    }
                }
            }
        }
    }
}
