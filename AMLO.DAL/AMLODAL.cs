﻿using AMLO.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMLO.DAL
{
    public class AMLODAL
    {
        public static string ClearData(string _conn, string type, DateTime? recordDate = null)
        {
            string res = string.Empty;

            string _query = "delete from watchlist where type = @type and recordDate = @recordDate";

            try
            {
                using (SqlConnection connection = new SqlConnection(_conn))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();

                        using (SqlCommand cmd = new SqlCommand(_query, connection))
                        {
                            try
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandTimeout = int.MaxValue; //int.MaxValue;
                                cmd.Connection = connection;

                                if (recordDate.HasValue)
                                {
                                    cmd.Parameters.AddWithValue("@recordDate", SqlDbType.Date).Value = recordDate;
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@recordDate", SqlDbType.Date).Value = DateTime.Now.ToString("yyyy-MM-dd");
                                }

                                cmd.Parameters.AddWithValue("@type", SqlDbType.VarChar).Value = type;

                                cmd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                if (connection.State != ConnectionState.Closed)
                                {
                                    connection.Close();
                                }
                                throw new Exception(ex.Message);
                            }
                        }

                        connection.Close();
                        res = "Success";
                    }
                }
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }

            return res;
        }

        public static string InsertDataWatchList(string _conn, WatchList watch, DateTime? recordDate = null)
        {
            string res = string.Empty;

            try
            {
                string _query = "INSERT INTO dbo.watchlist ";
                _query += "(recordDate, type,uid,commandNo,announceDate,announceDateEnd,groupName,entityType,status,yOrN,titleTh,firstNameTh,lastNameTh,middleName,nameFour,titleEn,firstNameEn,lastNameEn,originalName,birthday,birthCountry,birthCity,position,gender,job,occupation,nationality,personalID,passportNo,passportNo2,passportNo3,passportNo4,passportNo5,coRegisNo,taxNo,address,subdistrict,district,province,country,address2,subdistrict2,district2,province2,country2,otherName1,otherName2,otherName3,alias1,alias2,alias3,proofNo,proofDesc,createDate,updateDate) ";
                _query += "VALUES ";
                _query += "(@recordDate, @type,@uid,@commandNo,@announceDate,@announceDateEnd,@groupName,@entityType,@status,@yOrN,@titleTh,@firstNameTh,@lastNameTh,@middleName,@nameFour,@titleEn,@firstNameEn,@lastNameEn,@originalName,@birthday,@birthCountry,@birthCity,@position,@gender,@job,@occupation,@nationality,@personalID,@passportNo,@passportNo2,@passportNo3,@passportNo4,@passportNo5,@coRegisNo,@taxNo,@address,@subdistrict,@district,@province,@country,@address2,@subdistrict2,@district2,@province2,@country2,@otherName1,@otherName2,@otherName3,@alias1,@alias2,@alias3,@proofNo,@proofDesc,@createDate,@updateDate); ";

                using (SqlConnection connection = new SqlConnection(_conn))
                {
                    if (connection.State == ConnectionState.Closed)
                    {
                        connection.Open();
                        //SqlCommand cmd = new SqlCommand(query);

                        using (SqlCommand cmd = new SqlCommand(_query, connection))
                        {
                            try
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandTimeout = int.MaxValue; //int.MaxValue;
                                cmd.Connection = connection;

                                if (recordDate.HasValue)
                                {
                                    cmd.Parameters.AddWithValue("@recordDate", SqlDbType.Date).Value = recordDate;
                                    watch.recordDate = recordDate.GetValueOrDefault().ToString("yyyy-MM-dd");
                                }
                                else
                                {
                                    cmd.Parameters.AddWithValue("@recordDate", SqlDbType.Date).Value = DateTime.Now.ToString("yyyy-MM-dd");
                                    watch.recordDate = DateTime.Now.ToString("yyyy-MM-dd");
                                }

                                cmd.Parameters.AddWithValue("@type", SqlDbType.VarChar).Value = watch.type;
                                cmd.Parameters.AddWithValue("@uid", SqlDbType.VarChar).Value = watch.uid ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@commandNo", SqlDbType.VarChar).Value = watch.commandNo ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@announceDate", SqlDbType.Date).Value = watch.announceDate ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@announceDateEnd", SqlDbType.Date).Value = watch.announceDateEnd ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@groupName", SqlDbType.VarChar).Value = watch.groupName ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@entityType", SqlDbType.VarChar).Value = watch.entityType ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@status", SqlDbType.Int).Value = watch.status ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@yOrN", SqlDbType.VarChar).Value = watch.yOrN ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@titleTh", SqlDbType.VarChar).Value = watch.titleTh ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@firstNameTh", SqlDbType.VarChar).Value = watch.firstNameTh ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@lastNameTh", SqlDbType.VarChar).Value = watch.lastNameTh ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@middleName", SqlDbType.VarChar).Value = watch.middleName ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@nameFour", SqlDbType.VarChar).Value = watch.nameFour ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@titleEn", SqlDbType.VarChar).Value = watch.titleEn ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@firstNameEn", SqlDbType.VarChar).Value = watch.firstNameEn ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@lastNameEn", SqlDbType.VarChar).Value = watch.lastNameEn ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@originalName", SqlDbType.VarChar).Value = watch.originalName ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@birthday", SqlDbType.Date).Value = watch.birthday ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@birthCountry", SqlDbType.VarChar).Value = watch.birthCountry ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@birthCity", SqlDbType.VarChar).Value = watch.birthCity ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@position", SqlDbType.VarChar).Value = watch.position ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@gender", SqlDbType.VarChar).Value = watch.gender ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@job", SqlDbType.VarChar).Value = watch.job ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@occupation", SqlDbType.VarChar).Value = watch.occupation ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@nationality", SqlDbType.VarChar).Value = watch.nationality ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@personalID", SqlDbType.VarChar).Value = watch.personalID ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@passportNo", SqlDbType.VarChar).Value = watch.passportNo ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@passportNo2", SqlDbType.VarChar).Value = watch.passportNo2 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@passportNo3", SqlDbType.VarChar).Value = watch.passportNo3 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@passportNo4", SqlDbType.VarChar).Value = watch.passportNo4 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@passportNo5", SqlDbType.VarChar).Value = watch.passportNo5 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@coRegisNo", SqlDbType.VarChar).Value = watch.coRegisNo ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@taxNo", SqlDbType.VarChar).Value = watch.taxNo ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@address", SqlDbType.VarChar).Value = watch.address ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@subdistrict", SqlDbType.VarChar).Value = watch.subdistrict ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@district", SqlDbType.VarChar).Value = watch.district ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@province", SqlDbType.VarChar).Value = watch.province ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@country", SqlDbType.VarChar).Value = watch.country ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@address2", SqlDbType.VarChar).Value = watch.address2 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@subdistrict2", SqlDbType.VarChar).Value = watch.subdistrict2 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@district2", SqlDbType.VarChar).Value = watch.district2 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@province2", SqlDbType.VarChar).Value = watch.province2 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@country2", SqlDbType.VarChar).Value = watch.country2 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@otherName1", SqlDbType.VarChar).Value = watch.otherName1 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@otherName2", SqlDbType.VarChar).Value = watch.otherName2 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@otherName3", SqlDbType.VarChar).Value = watch.otherName3 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@alias1", SqlDbType.VarChar).Value = watch.alias1 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@alias2", SqlDbType.VarChar).Value = watch.alias2 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@alias3", SqlDbType.VarChar).Value = watch.alias3 ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@proofNo", SqlDbType.VarChar).Value = watch.proofNo ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@proofDesc", SqlDbType.VarChar).Value = watch.proofDesc ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@createDate", SqlDbType.Date).Value = watch.createDate ?? (object)DBNull.Value;
                                cmd.Parameters.AddWithValue("@updateDate", SqlDbType.Date).Value = watch.updateDate ?? (object)DBNull.Value;

                                cmd.ExecuteNonQuery();
                            }
                            catch (Exception ex)
                            {
                                if (connection.State != ConnectionState.Closed)
                                {
                                    connection.Close();
                                }
                                throw new Exception(ex.Message);
                            }
                        }

                        connection.Close();
                        res = "Success";
                    }
                   
                }
            }
            catch (Exception ex)
            {
                res = ex.Message;
            }

            return res;
        }

        public static List<kks_amlo_idnumber> GetDataFor_kks_amlo_idnumber(string _conn, string recordDate)
        {
            List<kks_amlo_idnumber> results = new List<kks_amlo_idnumber>();

            try
            {
                string query = string.Empty;
                query += " select ID entity_id, NULL sys_id, Case IsNull(PersonalID,'') When '' Then 'Passport' Else 'Identification Number' End id_type, Case IsNull(PersonalID,'') When '' Then passportNo Else personalID End id_value, ";
                query += "        Nationality id_country, NULL id_issuedate, NULL id_expiredate,  ";
                query += "        lTrim(rTrim(IsNull(Address,'')) + ' ' + rTrim(IsNull(Subdistrict,'')) + ' ' + rTrim(IsNull(District,'')) + ' ' + rTrim(IsNull(Province,''))  + ' ' + rTrim(IsNull(Country,''))) id_addn_ref, ";
                query += "        Upper(Type) info_source, commandNo batch_no, IsNull(AnnounceDate,CreateDate) batch_date, Type + '.json' xml_file, convert(varchar, recordDate, 23) date_input ";
                query += "   from watchlist ";
                query += "  where IsNull(PersonalID,'') + IsNull(passportNo,'') <> '' ";
                query += "    and recordDate = @recordDate ";

                using (SqlConnection connection = new SqlConnection(_conn))
                {
                    connection.Open();

                    SqlCommand cmd = new SqlCommand(query);
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@recordDate", SqlDbType.Date).Value = recordDate;

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        results.Add(new kks_amlo_idnumber
                        {
                            entity_id = reader["entity_id"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            sys_id = reader["sys_id"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            id_type = reader["id_type"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            id_value = reader["id_value"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            id_country = reader["id_country"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            id_issuedate = reader["id_issuedate"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            id_expiredate = reader["id_expiredate"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            id_addn_ref = reader["id_addn_ref"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            info_source = reader["info_source"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            batch_no = reader["batch_no"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            batch_date = reader["batch_date"].ToString(),
                            xml_file = reader["xml_file"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            date_input = reader["date_input"].ToString()
                        });
                    }
                    reader.Close();
                    connection.Close();
                }
            }
            catch
            {
                results = new List<kks_amlo_idnumber>();
            }

            return results;
        }

        public static List<kks_amlo_name> GetDataFor_kks_amlo_name(string _conn, string recordDate)
        {
            List<kks_amlo_name> results = new List<kks_amlo_name>();

            try
            {
                string query = string.Empty;
                query += " select UID entity_id, NULL sys_id, Case IsNull(firstNameTh,'') When '' Then 'Also Known As' Else 'Primary Name' End name_type, ";
                query += "        Case IsNull(firstNameTh,'') When '' Then firstNameEn Else firstNameTh End first_name, NULL middel_name,  ";
                query += "        Case IsNull(lastNameTh,'')  When '' Then lastNameEn  Else lastNameTh End surname, ";
                query += "        Case IsNull(titleTh,'')     When '' Then titleEn Else titleTh End suffix, NULL entity_name, ";
                query += "        IsNull(titleEN,'') + IsNull(firstNameEn,'') + IsNull(lastNameEn,'') english_name, ";
                query += "        Case IsNull(firstNameTh,'') When '' Then lTrim(IsNull(firstNameEn,'') + ' ' + IsNull(lastNameEn,'')) ";
                query += "                                            Else lTrim(IsNull(firstNameTh,'') + ' ' + IsNull(lastNameTh,'')) End single_string_name, ";
                query += "        Upper(Type) info_source, commandNo batch_no, IsNull(AnnounceDate,CreateDate) batch_date, Type + '.json' xml_file, convert(varchar, recordDate, 23) date_input ";
                query += "   from watchlist ";
                query += "  where IsNull(PersonalID,'') + IsNull(passportNo,'') = '' ";
                query += "    and recordDate = @recordDate ";

                using (SqlConnection connection = new SqlConnection(_conn))
                {
                    connection.Open();

                    SqlCommand cmd = new SqlCommand(query);
                    cmd.Connection = connection;
                    cmd.Parameters.AddWithValue("@recordDate", SqlDbType.Date).Value = recordDate;

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        results.Add(new kks_amlo_name
                        {
                            entity_id = reader["entity_id"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            sys_id = reader["sys_id"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            name_type = reader["name_type"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            first_name = reader["first_name"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            middel_name = reader["middel_name"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            surname = reader["surname"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            suffix = reader["suffix"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            entity_name = reader["entity_name"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            english_name = reader["english_name"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            single_string_name = reader["single_string_name"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            info_source = reader["info_source"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            batch_no = reader["batch_no"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            batch_date = reader["batch_date"].ToString(),
                            xml_file = reader["xml_file"].ToString().Replace("\n", "").Replace("\r", "").Trim(),
                            date_input = reader["date_input"].ToString()
                        });
                    }
                    reader.Close();
                    connection.Close();
                }
            }
            catch
            {
                results = new List<kks_amlo_name>();
            }

            return results;
        }

        public static string BasicAuth(string _conn)
        {
            string _query = string.Empty;
            _query += "select a.value param_user, b.value param_pass ";
            _query += "  from amloconfig a ";
            _query += "      ,amloconfig b ";
            _query += " where a.[key] = 'APIUser' ";
            _query += "   and b.[key] = 'APIPassword' ";

            using (SqlConnection connection = new SqlConnection(_conn))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(_query, connection))
                {
                    SqlDataReader result_reader = cmd.ExecuteReader();
                    if (result_reader.Read())
                    {
                        string param_user = result_reader["param_user"].ToString().Trim();
                        string param_pass = result_reader["param_pass"].ToString().Trim();

                        return Convert.ToBase64String(Encoding.ASCII.GetBytes($"{EncryptionUtility.DecryptString(param_user)}:{EncryptionUtility.DecryptString(param_pass)}"));
                    }
                    else
                    {
                        throw new Exception("ExecuteReader Fail.");
                    }
                }
            }
        }

        public static string XAPIKey(string _conn)
        {
            string _query = string.Empty;
            _query += "select value XAPIKey";
            _query += "  from amloconfig  ";;
            _query += " where [key] = 'XAPIKey' ";

            using (SqlConnection connection = new SqlConnection(_conn))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(_query, connection))
                {
                    SqlDataReader result_reader = cmd.ExecuteReader();
                    if (result_reader.Read())
                    {
                        string XAPIKey = result_reader["XAPIKey"].ToString().Trim();

                        return EncryptionUtility.DecryptString(XAPIKey);
                    }
                    else
                    {
                        throw new Exception("ExecuteReader Fail.");
                    }
                }
            }
        }
    }
}
