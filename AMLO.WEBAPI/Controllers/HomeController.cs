﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AMLO.WEBAPI.Controllers
{
    public class HomeController : Controller
    {
        public static NLog.Logger LogDB = NLog.LogManager.GetLogger("LogDB");
        public ActionResult Index()
        {
            LogDB.Info("Your application description page.");

            return View();
        }
    }
}