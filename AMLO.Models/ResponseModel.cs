﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMLO.Models
{
    public class ResponseModel
    {
        public string ReturnStatus { get; set; }
        public string ReturnMassage { get; set; }
        public string KeyID { get; set; }
        public string FileName { get; set; }
        public string mimeType { get; set; }
        public string Result { get; set; }
    }
}
