﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMLO.Models
{
    public class WatchList
    {
        public int id { get; set; }
        public string recordDate { get; set; }
        public string type { get; set; }
        public string uid { get; set; }
        public string commandNo { get; set; }
        public string announceDate { get; set; }
        public string announceDateEnd { get; set; }
        public string groupName { get; set; }
        public string entityType { get; set; }
        public string status { get; set; }
        public string yOrN { get; set; }
        public string titleTh { get; set; }
        public string firstNameTh { get; set; }
        public string lastNameTh { get; set; }
        public string middleName { get; set; }
        public string nameFour { get; set; }
        public string titleEn { get; set; }
        public string firstNameEn { get; set; }
        public string lastNameEn { get; set; }
        public string originalName { get; set; }
        public string birthday { get; set; }
        public string birthCountry { get; set; }
        public string birthCity { get; set; }
        public string position { get; set; }
        public string gender { get; set; }
        public string occupation { get; set; }
        public string job { get; set; }
        public string nationality { get; set; }
        public string personalID { get; set; }
        public string passportNo { get; set; }
        public string passportNo2 { get; set; }
        public string passportNo3 { get; set; }
        public string passportNo4 { get; set; }
        public string passportNo5 { get; set; }
        public string coRegisNo { get; set; }
        public string taxNo { get; set; }
        public string address { get; set; }
        public string subdistrict { get; set; }
        public string district { get; set; }
        public string province { get; set; }
        public string country { get; set; }
        public string address2 { get; set; }
        public string subdistrict2 { get; set; }
        public string district2 { get; set; }
        public string province2 { get; set; }
        public string country2 { get; set; }
        public string otherName1 { get; set; }
        public string otherName2 { get; set; }
        public string otherName3 { get; set; }
        public string alias1 { get; set; }
        public string alias2 { get; set; }
        public string alias3 { get; set; }
        public string proofNo { get; set; }
        public string proofDesc { get; set; }
        public string createDate { get; set; }
        public string updateDate { get; set; }

    }
}
