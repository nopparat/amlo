﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMLO.Models
{
    public class kks_amlo_idnumber
    {
        public string entity_id { get; set; }
        public string sys_id { get; set; }
        public string id_type { get; set; }
        public string id_value { get; set; }
        public string id_country { get; set; }
        public string id_issuedate { get; set; }
        public string id_expiredate { get; set; }
        public string id_addn_ref { get; set; }
        public string info_source { get; set; }
        public string batch_no { get; set; }
        public string batch_date { get; set; }
        public string xml_file { get; set; }
        public string date_input { get; set; }
    }

    public class kks_amlo_name
    {
        public string entity_id { get; set; }
        public string sys_id { get; set; }
        public string name_type { get; set; }
        public string first_name { get; set; }
        public string middel_name { get; set; }
        public string surname { get; set; }
        public string suffix { get; set; }
        public string entity_name { get; set; }
        public string english_name { get; set; }
        public string single_string_name { get; set; }
        public string info_source { get; set; }
        public string batch_no { get; set; }
        public string batch_date { get; set; }
        public string xml_file { get; set; }
        public string date_input { get; set; }
    }
}
