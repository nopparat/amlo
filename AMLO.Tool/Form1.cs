﻿using AMLO.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AMLO.Tool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            if (txtInput.Text == string.Empty)
            {
                MessageBox.Show("Please input value...");
            }
            else
            {
                string enc = EncryptionUtility.EncryptString(txtInput.Text);
                txtResult.Text = enc;
                btnCopy.Enabled = true;
                btnDecrypt.Enabled = true;

                //MessageBox.Show("Encrypt Success...");
            }
        }

        private void btnDecrypt_Click(object sender, EventArgs e)
        {
            if (txtInput.Text == string.Empty)
            {
                MessageBox.Show("Please input value...");
            }
            else
            {
                string enc = EncryptionUtility.DecryptString(txtInput.Text);
                txtResult.Text = enc;
                btnCopy.Enabled = true;
                btnEncrypt.Enabled = true;

                //MessageBox.Show("Decrypt Success...");
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtInput.Text = string.Empty;
            txtResult.Text = string.Empty;
            btnCopy.Enabled = false;
            btnEncrypt.Enabled = true;
            btnDecrypt.Enabled = true;

            //MessageBox.Show("Clear Input Success...");
        }
        
        private void btnCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtResult.Text);
            MessageBox.Show("Copy Result to Clipboard Success...");
        }
    }
}
