﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using AMLO.DAL;
using System.Configuration;
using System.IO;
using AMLO.Models;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Globalization;
using System.Runtime.InteropServices;

using System.Drawing;
using Console = Colorful.Console;

namespace AMLO.AutoAPI
{
    public class program
    {
        private static NLog.Logger logger = NLog.LogManager.GetLogger("Logfile");

        private static string inputDate = "";
        private static string inputFile = "";
        private static string FilePathRerun = "";

        public static List<kks_amlo_idnumber> _kks_amlo_idnumbers = new List<kks_amlo_idnumber>();
        public static List<kks_amlo_name> _kks_amlo_names = new List<kks_amlo_name>();

        private static int thailist_total = 0;
        private static int unlist_total = 0;
        private static int hr02_total = 0;
        private static int hr08_total = 0;

        private static int thailist_success = 0;
        private static int unlist_success = 0;
        private static int hr02_success = 0;
        private static int hr08_success = 0;

        static void Main(string[] args)
        {
            logger.Info($"Start Program Download AMLO");
            Maximize();

            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            Display();

            string isRerun = ConfigurationManager.AppSettings["Rerun"];
            if (isRerun.ToUpper().Equals("Y"))
            {
                logger.Info($"Process Rerun Read File");
                RerunProcess();
            }
            else
            {
                Console.WriteLine("Process Downloading API AMLO");
                logger.Info("Process Downloading API AMLO");

                HTTP_CALL_API();
            }
            
            Console.WriteLine("===============================================");
            Console.WriteLine("SendMail Log Monitor...");
            SendMail();
            Console.WriteLine("===============================================");
            Console.WriteLine("Complete Waiting Close Program...");
            System.Threading.Thread.Sleep((int)TimeSpan.FromSeconds(10).TotalMilliseconds);
        }

        static void HTTP_CALL_API()
        {
            Console.WriteLine("HTTP CALL API", Color.Green);

            try
            {
                var thailandlist = Common.HTTP_POST_API(ConfigurationManager.AppSettings["URL:THAILANDLIST"], "THAILANDLIST");

                thailist_total = thailandlist.Count;
                if (thailandlist.Count > 0)
                {
                    logger.Info($"Clear Data thailandlist : {DateTime.Now.Date.ToString("yyyy-MM-dd")}");
                    Console.WriteLine($"Clear Data thailandlist : {DateTime.Now.Date.ToString("yyyy-MM-dd")}");
                    AMLODAL.ClearData(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), "thailandlist");

                    logger.Info("Insert thailandlist Start");
                    Console.WriteLine("Insert thailandlist Start");
                    thailandlist.ForEach(item =>
                    {
                        item.type = "thailandlist";
                        var res = AMLODAL.InsertDataWatchList(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), item);
                        if (res != "Success")
                        {
                            logger.Error(res);
                            logger.Error(JsonConvert.SerializeObject(item).ToString());
                            Common.ErrorCount = Common.ErrorCount + 1;
                        }
                        else
                        {
                            thailist_success++;
                        }
                    });
                    logger.Info("Insert thailandlist End");
                    Console.WriteLine("Insert thailandlist End");
                }

                var unlist = Common.HTTP_POST_API(ConfigurationManager.AppSettings["URL:UNLIST"], "UNLIST");

                unlist_total = unlist.Count;
                if (unlist.Count > 0)
                {
                    logger.Info($"Clear Data unlist : {DateTime.Now.Date.ToString("yyyy-MM-dd")}");
                    Console.WriteLine($"Clear Data unlist : {DateTime.Now.Date.ToString("yyyy-MM-dd")}");
                    AMLODAL.ClearData(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), "unlist");

                    logger.Info("Insert unlist Start");
                    Console.WriteLine("Insert unlist Start");
                    unlist.ForEach(item =>
                    {
                        item.type = "unlist";
                        var res = AMLODAL.InsertDataWatchList(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), item);
                        if (res != "Success")
                        {
                            logger.Error(res);
                            logger.Error(JsonConvert.SerializeObject(item).ToString());
                            Common.ErrorCount = Common.ErrorCount + 1;
                        }
                        else
                        {
                            unlist_success = unlist_success + 1;
                        }
                    });
                    logger.Info("Insert unlist End");
                    Console.WriteLine("Insert unlist End");
                }

                var hr02list = Common.HTTP_POST_API(ConfigurationManager.AppSettings["URL:HR02"], "HR02");

                hr02_total = hr02list.Count;
                if (hr02list.Count > 0)
                {
                    logger.Info($"Clear Data hr02list : {DateTime.Now.Date.ToString("yyyy-MM-dd")}");
                    Console.WriteLine($"Clear Data hr02list : {DateTime.Now.Date.ToString("yyyy-MM-dd")}");
                    AMLODAL.ClearData(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), "hr02list");

                    logger.Info("Insert hr02list Start");
                    Console.WriteLine("Insert hr02list Start");
                    hr02list.ForEach(item =>
                    {
                        item.type = "hr02list";
                        var res = AMLODAL.InsertDataWatchList(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), item);
                        if (res != "Success")
                        {
                            logger.Error(res);
                            logger.Error(JsonConvert.SerializeObject(item).ToString());
                            Common.ErrorCount = Common.ErrorCount + 1;
                        }
                        else
                        {
                            hr02_success = hr02_success + 1;
                        }
                    });
                    logger.Info("Insert hr02list End");
                    Console.WriteLine("Insert hr02list End");
                }

                var hr08list = Common.HTTP_POST_API(ConfigurationManager.AppSettings["URL:HR08"], "HR08");

                hr08_total = hr08list.Count;
                if (hr08list.Count > 0)
                {
                    logger.Info($"Clear Data hr08list : {DateTime.Now.Date.ToString("yyyy-MM-dd")}");
                    Console.WriteLine($"Clear Data hr08list : {DateTime.Now.Date.ToString("yyyy-MM-dd")}");
                    AMLODAL.ClearData(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), "hr08list");

                    logger.Info("Insert hr08list Start");
                    Console.WriteLine("Insert hr08list Start");
                    hr08list.ForEach(item =>
                    {
                        item.type = "hr08list";
                        var res = AMLODAL.InsertDataWatchList(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), item);
                        if (res != "Success")
                        {
                            logger.Error(res);
                            logger.Error(JsonConvert.SerializeObject(item).ToString());
                            Common.ErrorCount = Common.ErrorCount + 1;
                        }
                        else
                        {
                            hr08_success = hr08_success + 1;
                        }
                    });
                    logger.Info("Insert hr08list End");
                    Console.WriteLine("Insert hr08list End");
                }

                ExportDataSBA(DateTime.Now.Date.ToString("yyyy-MM-dd"));
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                Console.WriteLine(ex.Message, Color.Red);
            }
        }

        static void RerunProcess()
        {
            Console.Write("Please Input Rerun Date (yyyy-MM-dd): ");
            inputDate = Console.ReadLine();
            CheckDirectoryExists(inputDate);

            Console.WriteLine();
            Console.Write("Please Input FileName (THAILANDLIST|UNLIST|HR02|HR08) : ");
            inputFile = Console.ReadLine();
            CheckFileName(inputFile);

            StreamFileInsertDB();
        }

        static void StreamFileInsertDB()
        {
            using (StreamReader stream = new StreamReader(FilePathRerun))
            {
                string json = stream.ReadToEnd();
                List<WatchList> responses = JsonConvert.DeserializeObject<List<WatchList>>(json);

                DateTime dtInputDate = DateTime.ParseExact(inputDate, "yyyy-MM-dd", new CultureInfo("en-US"));

                logger.Info($"Rerun Date : {inputDate}, Rerun File : {inputFile}");
                switch (inputFile)
                {
                    case "THAILANDLIST":
                        logger.Info($"Clear Data thailandlist : {dtInputDate.Date.ToString("yyyy-MM-dd")}");
                        Console.WriteLine($"Clear Data thailandlist : {dtInputDate.Date.ToString("yyyy-MM-dd")}");
                        AMLODAL.ClearData(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), "thailandlist", dtInputDate);

                        logger.Info("Insert thailandlist Start");
                        Console.WriteLine("Insert thailandlist Start");
                        thailist_total = responses.Count;
                        responses.ForEach(item =>
                        {
                            item.type = "thailandlist";
                            var res = AMLODAL.InsertDataWatchList(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), item, dtInputDate);
                            if (res != "Success")
                            {
                                logger.Error(res);
                                logger.Error(JsonConvert.SerializeObject(item));
                                Common.ErrorCount = Common.ErrorCount + 1;
                            }
                            else
                            {
                                thailist_success = thailist_success + 1;
                            }
                        });
                        logger.Info("Insert thailandlist End");
                        Console.WriteLine("Insert thailandlist End");
                        break;
                    case "UNLIST":
                        logger.Info($"Clear Data unlist : {dtInputDate.Date.ToString("yyyy-MM-dd")}");
                        Console.WriteLine($"Clear Data unlist : {dtInputDate.Date.ToString("yyyy-MM-dd")}");
                        AMLODAL.ClearData(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), "unlist", dtInputDate);

                        logger.Info("Insert unlist Start");
                        Console.WriteLine("Insert unlist Start");
                        unlist_total = responses.Count;
                        responses.ForEach(item =>
                        {
                            item.type = "unlist";
                            var res = AMLODAL.InsertDataWatchList(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), item, dtInputDate);
                            if (res != "Success")
                            {
                                logger.Error(res);
                                logger.Error(JsonConvert.SerializeObject(item));
                                Common.ErrorCount = Common.ErrorCount + 1;
                            }
                            else
                            {
                                unlist_success = unlist_success + 1;
                            }
                        });
                        logger.Info("Insert unlist End");
                        Console.WriteLine("Insert unlist End");
                        break;
                    case "HR02":
                        logger.Info($"Clear Data hr02list : {dtInputDate.Date.ToString("yyyy-MM-dd")}");
                        Console.WriteLine($"Clear Data hr02list : {dtInputDate.Date.ToString("yyyy-MM-dd")}");
                        AMLODAL.ClearData(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), "hr02list", dtInputDate);

                        logger.Info("Insert hr02list Start");
                        Console.WriteLine("Insert hr02list Start");
                        hr02_total = responses.Count;
                        responses.ForEach(item =>
                        {
                            item.type = "hr02list";
                            var res = AMLODAL.InsertDataWatchList(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), item, dtInputDate);
                            if (res != "Success")
                            {
                                logger.Error(res);
                                logger.Error(JsonConvert.SerializeObject(item));
                                Common.ErrorCount = Common.ErrorCount + 1;
                            }
                            else
                            {
                                hr02_success = hr02_success + 1;
                            }
                        });
                        logger.Info("Insert hr02list End");
                        Console.WriteLine("Insert hr02list End");
                        break;
                    case "HR08":
                        logger.Info($"Clear Data hr08list : {dtInputDate.Date.ToString("yyyy-MM-dd")}");
                        Console.WriteLine($"Clear Data hr08list : {dtInputDate.Date.ToString("yyyy-MM-dd")}");
                        AMLODAL.ClearData(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), "hr08list", dtInputDate);

                        logger.Info("Insert hr08list Start");
                        Console.WriteLine("Insert hr08list Start");
                        hr08_total = responses.Count;
                        responses.ForEach(item =>
                        {
                            item.type = "hr08list";
                            var res = AMLODAL.InsertDataWatchList(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), item, dtInputDate);
                            if (res != "Success")
                            {
                                logger.Error(res);
                                logger.Error(JsonConvert.SerializeObject(item));
                                Common.ErrorCount = Common.ErrorCount + 1;
                            }
                            else
                            {
                                hr08_success = hr08_success + 1;
                            }
                        });
                        logger.Info("Insert hr08list End");
                        Console.WriteLine("Insert hr08list End");
                        break;
                    default:
                        break;
                }

                Console.WriteLine();
                ExportDataSBA(dtInputDate.ToString("yyyy-MM-dd"));


                Console.WriteLine();
                Console.Write("Your Continue Process Rerun input Y or other key Close Program: ", Color.Yellow);

                string keyClose = Console.ReadLine();
                if (keyClose.ToUpper().Equals("Y"))
                {
                    Display();
                    RerunProcess();
                }
                else
                {
                    Environment.Exit(0);
                }
            }
        }

        static void CheckDirectoryExists(string _inputDate)
        {
            string path = IoHelper.BasePath + @"\download\" + _inputDate;

            if (!Directory.Exists(path))
            {
                Console.WriteLine("Folder File is not exists.", Color.Red);

                Console.Write("Please Input Rerun Date (yyyy-MM-dd) or N Close Program: ");
                inputDate = Console.ReadLine();

                if (inputDate.ToUpper().Equals("N"))
                {
                    Environment.Exit(0);
                }
                else
                {
                    CheckDirectoryExists(inputDate);
                }
            }
        }

        static void CheckFileName(string _inputFile)
        {
            inputFile = _inputFile.ToUpper();
            string[] names = { "THAILANDLIST", "UNLIST", "HR02", "HR08" };
            Array.Sort(names);

            if (names.Where(x => x == inputFile).Any())
            {
                CheckFileExits(inputFile);
            }
            else
            {
                Console.WriteLine("Input Name is not Correct", Color.Red);
                Console.Write("Please Input FileName (THAILANDLIST|UNLIST|HR02|HR08) or N Close Program: ");
                inputFile = Console.ReadLine();

                if (inputFile.ToUpper().Equals("N"))
                {
                    Environment.Exit(0);
                }
                else
                {
                    CheckFileName(inputFile);
                }
            }
        }

        static void CheckFileExits(string _inputFile)
        {
            string path = IoHelper.BasePath + @"\download\" + inputDate + @"\" + _inputFile + ".json";

            if (!File.Exists(path))
            {
                Console.WriteLine("File Not Found.", Color.Red);

                Console.Write("Please Input FileName (THAILANDLIST|UNLIST|HR02|HR08) : ");
                inputFile = Console.ReadLine();

                if (inputFile.ToUpper().Equals("N"))
                {
                    Environment.Exit(0);
                }
                else
                {
                    CheckFileName(inputFile);
                }
            }
            else
            {
                FilePathRerun = path;
            }
        }

        static void ExportDataSBA(string recordDate)
        {
            logger.Info("Query Data for SBA"); Console.WriteLine("Query Data for SBA", Color.DarkOrange);
            // _watches
            logger.Info("Get Data For kks_amlo_idnumber"); Console.WriteLine("Get Data For kks_amlo_idnumber", Color.DarkOrange);
            _kks_amlo_idnumbers = AMLODAL.GetDataFor_kks_amlo_idnumber(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), recordDate);

            logger.Info("Get Data For kks_amlo_name"); Console.WriteLine("Get Data For kks_amlo_name", Color.DarkOrange);
            _kks_amlo_names = AMLODAL.GetDataFor_kks_amlo_name(ConfigurationManager.ConnectionStrings["AMLODB"].ToString(), recordDate);

            try
            {
                logger.Info($"Export Data kks_amlo_idnumber {_kks_amlo_idnumbers.Count} Row and kks_amlo_name {_kks_amlo_names.Count} Row to SBA");
                SBADAL.ClearDataInsertData(ConfigurationManager.ConnectionStrings["SBA_Connection"].ToString(), recordDate, _kks_amlo_idnumbers, _kks_amlo_names);
                logger.Info("Export Data to SBA Complete.");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                Console.WriteLine($"Error {ex.Message}", Color.Red);
            }
        }

        static void SendMail()
        {
            List<string> filePathImports = new List<string>()
            {
                $@"{AppDomain.CurrentDomain.BaseDirectory}\logs\{DateTime.Now.Date.ToString("yyyy-MM-dd")}.log"
            };

            string contents = File.ReadAllText(filePathImports[0]);
            int errCount = new System.Text.RegularExpressions.Regex(@"(?i)ERROR | ").Matches(contents).Count;

            string html = string.Empty;
            html += $"<b><u>AMLO Monitor At</u></b>: {DateTime.Now.Date.ToString("yyyy-MM-dd")} <br/>";
            html += $"<h3>THAILANDLIST Total {thailist_total} Record, Success {thailist_success} Record</h3>";
            html += $"<h3>UNLIST Total {unlist_total} Record, Success {unlist_success} Record</h3>";
            html += $"<h3>HR02 Total {hr02_total} Record, Success {hr02_success} Record</h3>";
            html += $"<h3>HR08 Total {hr08_total} Record, Success {hr08_success} Record</h3>";

            int err = (thailist_total - thailist_success) + (unlist_total - unlist_success) + (hr02_total - hr02_success) + (hr08_total - hr08_success);

            html += $"&nbsp;&nbsp;&nbsp;&nbsp;<h1 style='color: red'>Error {err} Record</h1>";

            if (errCount > 0)
            {
                html += "<br/>";
                using (StreamReader reader = File.OpenText(filePathImports[0]))
                {
                    string line = "";
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.Contains("ERROR"))
                        {
                            html += "<p style='color: red; font-size: 11px'>" + line + "</p>";
                        }
                    }
                }
            }

            var body = System.Net.Mail.AlternateView.CreateAlternateViewFromString(html, new System.Net.Mime.ContentType("text/html"));
            var txtError = err > 0 ? $" Error {err} Record " : "";
            Common.SendMail(
            $"AMLO JOB Monitor: {DateTime.Now.Date.ToString("yyyy-MM-dd HH:mm:ss")}" + txtError
            , ConfigurationManager.AppSettings["Mail:ToDev"]
            , body
            , filePathImports
            );
        }

        static void Display()
        {
            Console.WriteAscii("AMLO", Color.FromArgb(208, 140, 255));
            Console.WriteLine();
        }

        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool ShowWindow(System.IntPtr hWnd, int cmdShow);

        private static void Maximize()
        {
            System.Diagnostics.Process p = System.Diagnostics.Process.GetCurrentProcess();
            ShowWindow(p.MainWindowHandle, 3); //SW_MAXIMIZE = 3
        }
    }
}
