﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using AMLO.DAL;
using System.Configuration;
using System.IO;
using AMLO.Models;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Security.Cryptography;
using System.Runtime.InteropServices;

using System.Drawing;
using Console = Colorful.Console;
using System.Net.Mail;
using NLog;
using System.Net.Configuration;

namespace AMLO.AutoAPI
{
    public class Common
    {
        public static int ErrorCount = 0;
        private static readonly string DownloadPath = IoHelper.BasePath + @"\download\" + DateTime.Now.ToString("yyyy-MM-dd");
        private static NLog.Logger logger = NLog.LogManager.GetLogger("Logfile");
        /// <summary>
        /// Ref: http://www.bouncycastle.org/csharp/
        /// </summary>
        /// <param name="_url"></param>
        /// <returns>
        /// Class List
        /// </returns>
        //public static async Task<List<WatchList>> HTTP_POST_API(string _url, string type)
        //{
        //    Console.WriteLine("POST: " + _url);

        //    using (HttpClient client = new HttpClient())
        //    {
        //        client.Timeout = new TimeSpan(0, 0, 120);
        //        client.DefaultRequestHeaders.Accept.Clear();
        //        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        //        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", AMLODAL.BasicAuth(ConfigurationManager.ConnectionStrings["AMLODB"].ConnectionString));
        //        client.DefaultRequestHeaders.Add("X-Api-key", AMLODAL.XAPIKey(ConfigurationManager.ConnectionStrings["AMLODB"].ConnectionString));

        //        HttpResponseMessage response = await client.PostAsync(_url, null);

        //        if (response.IsSuccessStatusCode)
        //        {
        //            string jsonMessage;
        //            using (Stream responseStream = await response.Content.ReadAsStreamAsync())
        //            {
        //                jsonMessage = new StreamReader(responseStream).ReadToEnd();
        //            }

        //            ResponseModel responseModel = (ResponseModel)JsonConvert.DeserializeObject(jsonMessage, typeof(ResponseModel));

        //            if (responseModel.ReturnStatus.ToUpper().Equals("TRUE"))
        //            {
        //                Console.WriteLine("ReturnStatus: " + responseModel.ReturnStatus.ToUpper(), Color.Green);

        //                var res = responseModel.Result;
        //                byte[] data = Convert.FromBase64String(res);
        //                string decodedString = Encoding.UTF8.GetString(data);

        //                string decrypted = CryptoHelper.DecryptPgpData(decodedString);

        //                List<WatchList> result = new List<WatchList>();
        //                result = JsonConvert.DeserializeObject<List<WatchList>>(decrypted);

        //                //write string to file
        //                CreateFile(result, type);

        //                client.Dispose();
        //                return result;
        //            }
        //            else
        //            {
        //                Console.WriteLine("ReturnStatus: " + responseModel.ReturnStatus.ToUpper(), Color.Red);
        //                Console.WriteLine("ReturnMassage: " + responseModel.ReturnMassage, Color.Red);
        //                client.Dispose();
        //                throw new Exception(responseModel.ReturnMassage);
        //            }
        //        }
        //        else
        //        {
        //            HttpContent content = response.Content;

        //            // ... Check Status Code                                
        //            Console.WriteLine("Response StatusCode: " + (int)response.StatusCode);

        //            // ... Read the string.
        //            string result = await content.ReadAsStringAsync();

        //            // ... Display the result.
        //            client.Dispose();
        //            throw new Exception(result);

        //        }
        //    }

        //}

        public static List<WatchList> HTTP_POST_API(string _url, string type)
        {
            Console.WriteLine("POST: " + _url);

            #region call responsewithdraw
            var resultCallBack = "";
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(_url);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Headers.Add("Authorization", "Basic " + AMLODAL.BasicAuth(ConfigurationManager.ConnectionStrings["AMLODB"].ConnectionString));
                httpWebRequest.Headers.Add("X-Api-key", AMLODAL.XAPIKey(ConfigurationManager.ConnectionStrings["AMLODB"].ConnectionString));

                var serializer = JsonConvert.SerializeObject(null);

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(serializer);
                    streamWriter.Flush();
                    streamWriter.Close();
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    resultCallBack = streamReader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                ErrorCount = ErrorCount + 1;
                resultCallBack = ex.Message;
                logger.Error(resultCallBack);
            }
            #endregion
            
            ResponseModel responseModel = (ResponseModel)JsonConvert.DeserializeObject(resultCallBack, typeof(ResponseModel));

            if (responseModel.ReturnStatus.ToUpper().Equals("TRUE"))
            {
                Console.WriteLine("ReturnStatus: " + responseModel.ReturnStatus.ToUpper(), Color.Green);

                var res = responseModel.Result;
                byte[] data = Convert.FromBase64String(res);
                string decodedString = Encoding.UTF8.GetString(data);

                string decrypted = CryptoHelper.DecryptPgpData(decodedString);
                decrypted = decrypted.Replace("\n", "").Replace("\r", string.Empty);
                List<WatchList> result = new List<WatchList>();
                result = JsonConvert.DeserializeObject<List<WatchList>>(decrypted);

                //write string to file
                CreateFile(result, type);

                return result;
            }
            else
            {
                ErrorCount = ErrorCount + 1;
                logger.Error("ReturnStatus: " + responseModel.ReturnStatus.ToUpper());
                logger.Error("ReturnMassage: " + responseModel.ReturnMassage);
                Console.WriteLine("ReturnStatus: " + responseModel.ReturnStatus.ToUpper(), Color.Red);
                Console.WriteLine("ReturnMassage: " + responseModel.ReturnMassage, Color.Red);

                throw new Exception(responseModel.ReturnMassage);
            }

        }

        private static bool IsFileLocked(string filePath)
        {
            try
            {
                using (File.Open(filePath, FileMode.Open)) { }
            }
            catch (IOException e)
            {
                var errorCode = Marshal.GetHRForException(e) & ((1 << 16) - 1);

                return errorCode == 32 || errorCode == 33;
            }

            return false;
        }

        public static void CreateFile(List<WatchList> _data, string fileName)
        {
            string WorkingFolder = DownloadPath;

            fileName = fileName + ".json";
            string InputFile = Path.Combine(WorkingFolder, fileName);

            try
            {
                if (!Directory.Exists(WorkingFolder))
                {
                    Directory.CreateDirectory(WorkingFolder);
                }

                // Check if file exists with its full path    
                if (File.Exists(InputFile))
                {
                    // If file found, delete it

                    if (IsFileLocked(InputFile))
                    {
                        throw new Exception("File Locked.");
                    }

                    File.Delete(InputFile);
                    Console.WriteLine("TempFile deleted.", Color.Green);
                }
                else Console.WriteLine("TempFile not found", Color.Green);

                string json = JsonConvert.SerializeObject(_data.ToArray(), Formatting.Indented);
                System.IO.File.WriteAllText(InputFile, json);

                Console.WriteLine($"Create File {InputFile} Success", Color.Green);
            }
            catch (IOException ioExp)
            {
                ErrorCount = ErrorCount + 1;
                logger.Error(ioExp.Message);
                Console.WriteLine(ioExp.Message);
                throw new Exception(ioExp.Message);
            }
        }

        public static void SendMail(string subject, string emailTo, AlternateView htmlbody, List<string> attachlist, List<string> emailBCC = null, List<string> emailCC = null)
        {
            LogManager.ThrowExceptions = true;
            SmtpSection section = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");

            string smtpAddress = section.Network.Host;
            int portNumber = section.Network.Port;
            bool enableSSL = false;

            string emailFrom = section.From;
            string password = "";

            //  string body = htmlbody;
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress(emailFrom);
                    mail.Headers.Add("Disposition-Notification-To", section.From);
                    //mail.To.Add(emailTo);

                    foreach (var address in emailTo.Split(new[] { ";" }, StringSplitOptions.RemoveEmptyEntries))
                    {
                        mail.To.Add(address);
                    }

                    if (emailCC != null)
                        foreach (var cc in emailCC)
                        {
                            mail.CC.Add(cc);
                        }
                    if (emailBCC != null)
                        foreach (var bcc in emailBCC)
                        {
                            mail.Bcc.Add(bcc);
                        }

                    mail.Subject = subject;
                    mail.AlternateViews.Add(htmlbody);
                    mail.IsBodyHtml = true;

                    if (attachlist != null)
                    {
                        List<Stream> streams = new List<Stream>();

                        foreach (string file in attachlist)
                        {
                            Stream attachmentStream = File.OpenRead(file);
                            mail.Attachments.Add(new Attachment(attachmentStream, Path.GetFileName(file)));
                        }
                    }
                    using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                    {
                        smtp.Credentials = new NetworkCredential(emailFrom, password);
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        smtp.EnableSsl = enableSSL;
                        smtp.Send(mail);

                    }
                }
            }
            catch (SmtpFailedRecipientException ex)
            {
                throw new Exception("Send Fail Email " + emailTo, ex);
            }
            catch (Exception ex)
            {
                throw new Exception("Send Fail Email " + emailTo, ex);
            }
        }
    }
}
